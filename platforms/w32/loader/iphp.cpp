#ifdef BLT_USE_IPHLPAPI

#define WIN32_LEAN_AND_MEAN 1
#include "InitState.h"
#include <windows.h>

#include <memory>

#pragma pack(1)

FARPROC p[267] = {0};

namespace pd2hook
{
	namespace
	{
		struct DllState
		{
			HINSTANCE hLThis = nullptr;
			HMODULE hL = nullptr;
		};

		struct DllStateDestroyer
		{
			void operator()(DllState* state)
			{
				DestroyStates();

				if (state && state->hL)
				{
					FreeLibrary(state->hL);
				}
			}
		};

		std::unique_ptr<DllState, DllStateDestroyer> State;
	} // namespace
} // namespace pd2hook

BOOL WINAPI DllMain(HINSTANCE hInst, DWORD reason, LPVOID)
{

	if (reason == DLL_PROCESS_ATTACH)
	{
		char bufd[200];
		GetSystemDirectory(bufd, 200);
		strcat_s(bufd, "\\IPHLPAPI.dll");

		pd2hook::State.reset(new pd2hook::DllState());
		pd2hook::State->hLThis = hInst;
		pd2hook::State->hL = LoadLibrary(bufd);
		HMODULE hL = pd2hook::State->hL;

		hL = LoadLibrary(bufd);
		if (!hL)
			return false;

		p[0] = GetProcAddress(hL, "AddIPAddress");
		p[1] = GetProcAddress(hL, "AllocateAndGetInterfaceInfoFromStack");
		p[2] = GetProcAddress(hL, "AllocateAndGetIpAddrTableFromStack");
		p[3] = GetProcAddress(hL, "CancelIPChangeNotify");
		p[4] = GetProcAddress(hL, "CancelMibChangeNotify2");
		p[5] = GetProcAddress(hL, "CloseCompartment");
		p[6] = GetProcAddress(hL, "CloseGetIPPhysicalInterfaceForDestination");
		p[7] = GetProcAddress(hL, "ConvertCompartmentGuidToId");
		p[8] = GetProcAddress(hL, "ConvertCompartmentIdToGuid");
		p[9] = GetProcAddress(hL, "ConvertGuidToStringA");
		p[10] = GetProcAddress(hL, "ConvertGuidToStringW");
		p[11] = GetProcAddress(hL, "ConvertInterfaceAliasToLuid");
		p[12] = GetProcAddress(hL, "ConvertInterfaceGuidToLuid");
		p[13] = GetProcAddress(hL, "ConvertInterfaceIndexToLuid");
		p[14] = GetProcAddress(hL, "ConvertInterfaceLuidToAlias");
		p[15] = GetProcAddress(hL, "ConvertInterfaceLuidToGuid");
		p[16] = GetProcAddress(hL, "ConvertInterfaceLuidToIndex");
		p[17] = GetProcAddress(hL, "ConvertInterfaceLuidToNameA");
		p[18] = GetProcAddress(hL, "ConvertInterfaceLuidToNameW");
		p[19] = GetProcAddress(hL, "ConvertInterfaceNameToLuidA");
		p[20] = GetProcAddress(hL, "ConvertInterfaceNameToLuidW");
		p[21] = GetProcAddress(hL, "ConvertInterfacePhysicalAddressToLuid");
		p[22] = GetProcAddress(hL, "ConvertIpv4MaskToLength");
		p[23] = GetProcAddress(hL, "ConvertLengthToIpv4Mask");
		p[24] = GetProcAddress(hL, "ConvertRemoteInterfaceAliasToLuid");
		p[25] = GetProcAddress(hL, "ConvertRemoteInterfaceGuidToLuid");
		p[26] = GetProcAddress(hL, "ConvertRemoteInterfaceIndexToLuid");
		p[27] = GetProcAddress(hL, "ConvertRemoteInterfaceLuidToAlias");
		p[28] = GetProcAddress(hL, "ConvertRemoteInterfaceLuidToGuid");
		p[29] = GetProcAddress(hL, "ConvertRemoteInterfaceLuidToIndex");
		p[30] = GetProcAddress(hL, "ConvertStringToGuidA");
		p[31] = GetProcAddress(hL, "ConvertStringToGuidW");
		p[32] = GetProcAddress(hL, "ConvertStringToInterfacePhysicalAddress");
		p[33] = GetProcAddress(hL, "CreateAnycastIpAddressEntry");
		p[34] = GetProcAddress(hL, "CreateIpForwardEntry");
		p[35] = GetProcAddress(hL, "CreateIpForwardEntry2");
		p[36] = GetProcAddress(hL, "CreateIpNetEntry");
		p[37] = GetProcAddress(hL, "CreateIpNetEntry2");
		p[38] = GetProcAddress(hL, "CreatePersistentTcpPortReservation");
		p[39] = GetProcAddress(hL, "CreatePersistentUdpPortReservation");
		p[40] = GetProcAddress(hL, "CreateProxyArpEntry");
		p[41] = GetProcAddress(hL, "CreateSortedAddressPairs");
		p[42] = GetProcAddress(hL, "CreateUnicastIpAddressEntry");
		p[43] = GetProcAddress(hL, "DeleteAnycastIpAddressEntry");
		p[44] = GetProcAddress(hL, "DeleteIPAddress");
		p[45] = GetProcAddress(hL, "DeleteIpForwardEntry");
		p[46] = GetProcAddress(hL, "DeleteIpForwardEntry2");
		p[47] = GetProcAddress(hL, "DeleteIpNetEntry");
		p[48] = GetProcAddress(hL, "DeleteIpNetEntry2");
		p[49] = GetProcAddress(hL, "DeletePersistentTcpPortReservation");
		p[50] = GetProcAddress(hL, "DeletePersistentUdpPortReservation");
		p[51] = GetProcAddress(hL, "DeleteProxyArpEntry");
		p[52] = GetProcAddress(hL, "DeleteUnicastIpAddressEntry");
		p[53] = GetProcAddress(hL, "DisableMediaSense");
		p[54] = GetProcAddress(hL, "EnableRouter");
		p[55] = GetProcAddress(hL, "FlushIpNetTable");
		p[56] = GetProcAddress(hL, "FlushIpNetTable2");
		p[57] = GetProcAddress(hL, "FlushIpPathTable");
		p[58] = GetProcAddress(hL, "FreeMibTable");
		p[59] = GetProcAddress(hL, "GetAdapterIndex");
		p[60] = GetProcAddress(hL, "GetAdapterOrderMap");
		p[61] = GetProcAddress(hL, "GetAdaptersAddresses");
		p[62] = GetProcAddress(hL, "GetAdaptersInfo");
		p[63] = GetProcAddress(hL, "GetAnycastIpAddressEntry");
		p[64] = GetProcAddress(hL, "GetAnycastIpAddressTable");
		p[65] = GetProcAddress(hL, "GetBestInterface");
		p[66] = GetProcAddress(hL, "GetBestInterfaceEx");
		p[67] = GetProcAddress(hL, "GetBestRoute");
		p[68] = GetProcAddress(hL, "GetBestRoute2");
		p[69] = GetProcAddress(hL, "GetCurrentThreadCompartmentId");
		p[70] = GetProcAddress(hL, "GetExtendedTcpTable");
		p[71] = GetProcAddress(hL, "GetExtendedUdpTable");
		p[72] = GetProcAddress(hL, "GetFriendlyIfIndex");
		p[73] = GetProcAddress(hL, "GetIcmpStatistics");
		p[74] = GetProcAddress(hL, "GetIcmpStatisticsEx");
		p[75] = GetProcAddress(hL, "GetIfEntry");
		p[76] = GetProcAddress(hL, "GetIfEntry2");
		p[77] = GetProcAddress(hL, "GetIfStackTable");
		p[78] = GetProcAddress(hL, "GetIfTable");
		p[79] = GetProcAddress(hL, "GetIfTable2");
		p[80] = GetProcAddress(hL, "GetIfTable2Ex");
		p[81] = GetProcAddress(hL, "GetInterfaceInfo");
		p[82] = GetProcAddress(hL, "GetInvertedIfStackTable");
		p[83] = GetProcAddress(hL, "GetIpAddrTable");
		p[84] = GetProcAddress(hL, "GetIpErrorString");
		p[85] = GetProcAddress(hL, "GetIpForwardEntry2");
		p[86] = GetProcAddress(hL, "GetIpForwardTable");
		p[87] = GetProcAddress(hL, "GetIpForwardTable2");
		p[88] = GetProcAddress(hL, "GetIpInterfaceEntry");
		p[89] = GetProcAddress(hL, "GetIpInterfaceTable");
		p[90] = GetProcAddress(hL, "GetIpNetEntry2");
		p[91] = GetProcAddress(hL, "GetIpNetTable");
		p[92] = GetProcAddress(hL, "GetIpNetTable2");
		p[93] = GetProcAddress(hL, "GetIpNetworkConnectionBandwidthEstimates");
		p[94] = GetProcAddress(hL, "GetIpPathEntry");
		p[95] = GetProcAddress(hL, "GetIpPathTable");
		p[96] = GetProcAddress(hL, "GetIpStatistics");
		p[97] = GetProcAddress(hL, "GetIpStatisticsEx");
		p[98] = GetProcAddress(hL, "GetMulticastIpAddressEntry");
		p[99] = GetProcAddress(hL, "GetMulticastIpAddressTable");
		p[100] = GetProcAddress(hL, "GetNetworkInformation");
		p[101] = GetProcAddress(hL, "GetNetworkParams");
		p[102] = GetProcAddress(hL, "GetNumberOfInterfaces");
		p[103] = GetProcAddress(hL, "GetOwnerModuleFromPidAndInfo");
		p[104] = GetProcAddress(hL, "GetOwnerModuleFromTcp6Entry");
		p[105] = GetProcAddress(hL, "GetOwnerModuleFromTcpEntry");
		p[106] = GetProcAddress(hL, "GetOwnerModuleFromUdp6Entry");
		p[107] = GetProcAddress(hL, "GetOwnerModuleFromUdpEntry");
		p[108] = GetProcAddress(hL, "GetPerAdapterInfo");
		p[109] = GetProcAddress(hL, "GetPerTcp6ConnectionEStats");
		p[110] = GetProcAddress(hL, "GetPerTcp6ConnectionStats");
		p[111] = GetProcAddress(hL, "GetPerTcpConnectionEStats");
		p[112] = GetProcAddress(hL, "GetPerTcpConnectionStats");
		p[113] = GetProcAddress(hL, "GetRTTAndHopCount");
		p[114] = GetProcAddress(hL, "GetSessionCompartmentId");
		p[115] = GetProcAddress(hL, "GetTcp6Table");
		p[116] = GetProcAddress(hL, "GetTcp6Table2");
		p[117] = GetProcAddress(hL, "GetTcpStatistics");
		p[118] = GetProcAddress(hL, "GetTcpStatisticsEx");
		p[119] = GetProcAddress(hL, "GetTcpTable");
		p[120] = GetProcAddress(hL, "GetTcpTable2");
		p[121] = GetProcAddress(hL, "GetTeredoPort");
		p[122] = GetProcAddress(hL, "GetUdp6Table");
		p[123] = GetProcAddress(hL, "GetUdpStatistics");
		p[124] = GetProcAddress(hL, "GetUdpStatisticsEx");
		p[125] = GetProcAddress(hL, "GetUdpTable");
		p[126] = GetProcAddress(hL, "GetUniDirectionalAdapterInfo");
		p[127] = GetProcAddress(hL, "GetUnicastIpAddressEntry");
		p[128] = GetProcAddress(hL, "GetUnicastIpAddressTable");
		p[129] = GetProcAddress(hL, "Icmp6CreateFile");
		p[130] = GetProcAddress(hL, "Icmp6ParseReplies");
		p[131] = GetProcAddress(hL, "Icmp6SendEcho2");
		p[132] = GetProcAddress(hL, "IcmpCloseHandle");
		p[133] = GetProcAddress(hL, "IcmpCreateFile");
		p[134] = GetProcAddress(hL, "IcmpParseReplies");
		p[135] = GetProcAddress(hL, "IcmpSendEcho");
		p[136] = GetProcAddress(hL, "IcmpSendEcho2");
		p[137] = GetProcAddress(hL, "IcmpSendEcho2Ex");
		p[138] = GetProcAddress(hL, "InitializeIpForwardEntry");
		p[139] = GetProcAddress(hL, "InitializeIpInterfaceEntry");
		p[140] = GetProcAddress(hL, "InitializeUnicastIpAddressEntry");
		p[141] = GetProcAddress(hL, "InternalCleanupPersistentStore");
		p[142] = GetProcAddress(hL, "InternalCreateAnycastIpAddressEntry");
		p[143] = GetProcAddress(hL, "InternalCreateIpForwardEntry");
		p[144] = GetProcAddress(hL, "InternalCreateIpForwardEntry2");
		p[145] = GetProcAddress(hL, "InternalCreateIpNetEntry");
		p[146] = GetProcAddress(hL, "InternalCreateIpNetEntry2");
		p[147] = GetProcAddress(hL, "InternalCreateUnicastIpAddressEntry");
		p[148] = GetProcAddress(hL, "InternalDeleteAnycastIpAddressEntry");
		p[149] = GetProcAddress(hL, "InternalDeleteIpForwardEntry");
		p[150] = GetProcAddress(hL, "InternalDeleteIpForwardEntry2");
		p[151] = GetProcAddress(hL, "InternalDeleteIpNetEntry");
		p[152] = GetProcAddress(hL, "InternalDeleteIpNetEntry2");
		p[153] = GetProcAddress(hL, "InternalDeleteUnicastIpAddressEntry");
		p[154] = GetProcAddress(hL, "InternalFindInterfaceByAddress");
		p[155] = GetProcAddress(hL, "InternalGetAnycastIpAddressEntry");
		p[156] = GetProcAddress(hL, "InternalGetAnycastIpAddressTable");
		p[157] = GetProcAddress(hL, "InternalGetForwardIpTable2");
		p[158] = GetProcAddress(hL, "InternalGetIPPhysicalInterfaceForDestination");
		p[159] = GetProcAddress(hL, "InternalGetIfEntry2");
		p[160] = GetProcAddress(hL, "InternalGetIfTable");
		p[161] = GetProcAddress(hL, "InternalGetIfTable2");
		p[162] = GetProcAddress(hL, "InternalGetIpAddrTable");
		p[163] = GetProcAddress(hL, "InternalGetIpForwardEntry2");
		p[164] = GetProcAddress(hL, "InternalGetIpForwardTable");
		p[165] = GetProcAddress(hL, "InternalGetIpInterfaceEntry");
		p[166] = GetProcAddress(hL, "InternalGetIpInterfaceTable");
		p[167] = GetProcAddress(hL, "InternalGetIpNetEntry2");
		p[168] = GetProcAddress(hL, "InternalGetIpNetTable");
		p[169] = GetProcAddress(hL, "InternalGetIpNetTable2");
		p[170] = GetProcAddress(hL, "InternalGetMulticastIpAddressEntry");
		p[171] = GetProcAddress(hL, "InternalGetMulticastIpAddressTable");
		p[172] = GetProcAddress(hL, "InternalGetRtcSlotInformation");
		p[173] = GetProcAddress(hL, "InternalGetTcp6Table2");
		p[174] = GetProcAddress(hL, "InternalGetTcp6TableWithOwnerModule");
		p[175] = GetProcAddress(hL, "InternalGetTcp6TableWithOwnerPid");
		p[176] = GetProcAddress(hL, "InternalGetTcpTable");
		p[177] = GetProcAddress(hL, "InternalGetTcpTable2");
		p[178] = GetProcAddress(hL, "InternalGetTcpTableEx");
		p[179] = GetProcAddress(hL, "InternalGetTcpTableWithOwnerModule");
		p[180] = GetProcAddress(hL, "InternalGetTcpTableWithOwnerPid");
		p[181] = GetProcAddress(hL, "InternalGetTunnelPhysicalAdapter");
		p[182] = GetProcAddress(hL, "InternalGetUdp6TableWithOwnerModule");
		p[183] = GetProcAddress(hL, "InternalGetUdp6TableWithOwnerPid");
		p[184] = GetProcAddress(hL, "InternalGetUdpTable");
		p[185] = GetProcAddress(hL, "InternalGetUdpTableEx");
		p[186] = GetProcAddress(hL, "InternalGetUdpTableWithOwnerModule");
		p[187] = GetProcAddress(hL, "InternalGetUdpTableWithOwnerPid");
		p[188] = GetProcAddress(hL, "InternalGetUnicastIpAddressEntry");
		p[189] = GetProcAddress(hL, "InternalGetUnicastIpAddressTable");
		p[190] = GetProcAddress(hL, "InternalIcmpCreateFileEx");
		p[191] = GetProcAddress(hL, "InternalSetIfEntry");
		p[192] = GetProcAddress(hL, "InternalSetIpForwardEntry");
		p[193] = GetProcAddress(hL, "InternalSetIpForwardEntry2");
		p[194] = GetProcAddress(hL, "InternalSetIpInterfaceEntry");
		p[195] = GetProcAddress(hL, "InternalSetIpNetEntry");
		p[196] = GetProcAddress(hL, "InternalSetIpNetEntry2");
		p[197] = GetProcAddress(hL, "InternalSetIpStats");
		p[198] = GetProcAddress(hL, "InternalSetTcpEntry");
		p[199] = GetProcAddress(hL, "InternalSetTeredoPort");
		p[200] = GetProcAddress(hL, "InternalSetUnicastIpAddressEntry");
		p[201] = GetProcAddress(hL, "IpReleaseAddress");
		p[202] = GetProcAddress(hL, "IpRenewAddress");
		p[203] = GetProcAddress(hL, "LookupPersistentTcpPortReservation");
		p[204] = GetProcAddress(hL, "LookupPersistentUdpPortReservation");
		p[205] = GetProcAddress(hL, "NTPTimeToNTFileTime");
		p[206] = GetProcAddress(hL, "NTTimeToNTPTime");
		p[207] = GetProcAddress(hL, "NhGetGuidFromInterfaceName");
		p[208] = GetProcAddress(hL, "NhGetInterfaceDescriptionFromGuid");
		p[209] = GetProcAddress(hL, "NhGetInterfaceNameFromDeviceGuid");
		p[210] = GetProcAddress(hL, "NhGetInterfaceNameFromGuid");
		p[211] = GetProcAddress(hL, "NhpAllocateAndGetInterfaceInfoFromStack");
		p[212] = GetProcAddress(hL, "NotifyAddrChange");
		p[213] = GetProcAddress(hL, "NotifyCompartmentChange");
		p[214] = GetProcAddress(hL, "NotifyIpInterfaceChange");
		p[215] = GetProcAddress(hL, "NotifyRouteChange");
		p[216] = GetProcAddress(hL, "NotifyRouteChange2");
		p[217] = GetProcAddress(hL, "NotifyStableUnicastIpAddressTable");
		p[218] = GetProcAddress(hL, "NotifyTeredoPortChange");
		p[219] = GetProcAddress(hL, "NotifyUnicastIpAddressChange");
		p[220] = GetProcAddress(hL, "OpenCompartment");
		p[221] = GetProcAddress(hL, "ParseNetworkString");
		p[222] = GetProcAddress(hL, "ResolveIpNetEntry2");
		p[223] = GetProcAddress(hL, "ResolveNeighbor");
		p[224] = GetProcAddress(hL, "RestoreMediaSense");
		p[225] = GetProcAddress(hL, "SendARP");
		p[226] = GetProcAddress(hL, "SetAdapterIpAddress");
		p[227] = GetProcAddress(hL, "SetCurrentThreadCompartmentId");
		p[228] = GetProcAddress(hL, "SetIfEntry");
		p[229] = GetProcAddress(hL, "SetIpForwardEntry");
		p[230] = GetProcAddress(hL, "SetIpForwardEntry2");
		p[231] = GetProcAddress(hL, "SetIpInterfaceEntry");
		p[232] = GetProcAddress(hL, "SetIpNetEntry");
		p[233] = GetProcAddress(hL, "SetIpNetEntry2");
		p[234] = GetProcAddress(hL, "SetIpStatistics");
		p[235] = GetProcAddress(hL, "SetIpStatisticsEx");
		p[236] = GetProcAddress(hL, "SetIpTTL");
		p[237] = GetProcAddress(hL, "SetNetworkInformation");
		p[238] = GetProcAddress(hL, "SetPerTcp6ConnectionEStats");
		p[239] = GetProcAddress(hL, "SetPerTcp6ConnectionStats");
		p[240] = GetProcAddress(hL, "SetPerTcpConnectionEStats");
		p[241] = GetProcAddress(hL, "SetPerTcpConnectionStats");
		p[242] = GetProcAddress(hL, "SetSessionCompartmentId");
		p[243] = GetProcAddress(hL, "SetTcpEntry");
		p[244] = GetProcAddress(hL, "SetUnicastIpAddressEntry");
		p[245] = GetProcAddress(hL, "UnenableRouter");
		p[246] = GetProcAddress(hL, "_PfAddFiltersToInterface@24");
		p[247] = GetProcAddress(hL, "_PfAddGlobalFilterToInterface@8");
		p[248] = GetProcAddress(hL, "_PfBindInterfaceToIPAddress@12");
		p[249] = GetProcAddress(hL, "_PfBindInterfaceToIndex@16");
		p[250] = GetProcAddress(hL, "_PfCreateInterface@24");
		p[251] = GetProcAddress(hL, "_PfDeleteInterface@4");
		p[252] = GetProcAddress(hL, "_PfDeleteLog@0");
		p[253] = GetProcAddress(hL, "_PfGetInterfaceStatistics@16");
		p[254] = GetProcAddress(hL, "_PfMakeLog@4");
		p[255] = GetProcAddress(hL, "_PfRebindFilters@8");
		p[256] = GetProcAddress(hL, "_PfRemoveFilterHandles@12");
		p[257] = GetProcAddress(hL, "_PfRemoveFiltersFromInterface@20");
		p[258] = GetProcAddress(hL, "_PfRemoveGlobalFilterFromInterface@8");
		p[259] = GetProcAddress(hL, "_PfSetLogBuffer@28");
		p[260] = GetProcAddress(hL, "_PfTestPacket@20");
		p[261] = GetProcAddress(hL, "_PfUnBindInterface@4");
		p[262] = GetProcAddress(hL, "do_echo_rep");
		p[263] = GetProcAddress(hL, "do_echo_req");
		p[264] = GetProcAddress(hL, "if_indextoname");
		p[265] = GetProcAddress(hL, "if_nametoindex");
		p[266] = GetProcAddress(hL, "register_icmp");

		pd2hook::InitiateStates();
	}
	if (reason == DLL_PROCESS_DETACH)
	{
		pd2hook::State.reset();
	}

	return 1;
}

extern "C"
{
	FARPROC PA = 0;
	int jumpToPA();

	// AddIPAddress
	void __stdcall __E__0__()
	{
		PA = p[0];
		jumpToPA();
	}

	// AllocateAndGetInterfaceInfoFromStack
	void __stdcall __E__1__()
	{
		PA = p[1];
		jumpToPA();
	}

	// AllocateAndGetIpAddrTableFromStack
	void __stdcall __E__2__()
	{
		PA = p[2];
		jumpToPA();
	}

	// CancelIPChangeNotify
	void __stdcall __E__3__()
	{
		PA = p[3];
		jumpToPA();
	}

	// CancelMibChangeNotify2
	void __stdcall __E__4__()
	{
		PA = p[4];
		jumpToPA();
	}

	// CloseCompartment
	void __stdcall __E__5__()
	{
		PA = p[5];
		jumpToPA();
	}

	// CloseGetIPPhysicalInterfaceForDestination
	void __stdcall __E__6__()
	{
		PA = p[6];
		jumpToPA();
	}

	// ConvertCompartmentGuidToId
	void __stdcall __E__7__()
	{
		PA = p[7];
		jumpToPA();
	}

	// ConvertCompartmentIdToGuid
	void __stdcall __E__8__()
	{
		PA = p[8];
		jumpToPA();
	}

	// ConvertGuidToStringA
	void __stdcall __E__9__()
	{
		PA = p[9];
		jumpToPA();
	}

	// ConvertGuidToStringW
	void __stdcall __E__10__()
	{
		PA = p[10];
		jumpToPA();
	}

	// ConvertInterfaceAliasToLuid
	void __stdcall __E__11__()
	{
		PA = p[11];
		jumpToPA();
	}

	// ConvertInterfaceGuidToLuid
	void __stdcall __E__12__()
	{
		PA = p[12];
		jumpToPA();
	}

	// ConvertInterfaceIndexToLuid
	void __stdcall __E__13__()
	{
		PA = p[13];
		jumpToPA();
	}

	// ConvertInterfaceLuidToAlias
	void __stdcall __E__14__()
	{
		PA = p[14];
		jumpToPA();
	}

	// ConvertInterfaceLuidToGuid
	void __stdcall __E__15__()
	{
		PA = p[15];
		jumpToPA();
	}

	// ConvertInterfaceLuidToIndex
	void __stdcall __E__16__()
	{
		PA = p[16];
		jumpToPA();
	}

	// ConvertInterfaceLuidToNameA
	void __stdcall __E__17__()
	{
		PA = p[17];
		jumpToPA();
	}

	// ConvertInterfaceLuidToNameW
	void __stdcall __E__18__()
	{
		PA = p[18];
		jumpToPA();
	}

	// ConvertInterfaceNameToLuidA
	void __stdcall __E__19__()
	{
		PA = p[19];
		jumpToPA();
	}

	// ConvertInterfaceNameToLuidW
	void __stdcall __E__20__()
	{
		PA = p[20];
		jumpToPA();
	}

	// ConvertInterfacePhysicalAddressToLuid
	void __stdcall __E__21__()
	{
		PA = p[21];
		jumpToPA();
	}

	// ConvertIpv4MaskToLength
	void __stdcall __E__22__()
	{
		PA = p[22];
		jumpToPA();
	}

	// ConvertLengthToIpv4Mask
	void __stdcall __E__23__()
	{
		PA = p[23];
		jumpToPA();
	}

	// ConvertRemoteInterfaceAliasToLuid
	void __stdcall __E__24__()
	{
		PA = p[24];
		jumpToPA();
	}

	// ConvertRemoteInterfaceGuidToLuid
	void __stdcall __E__25__()
	{
		PA = p[25];
		jumpToPA();
	}

	// ConvertRemoteInterfaceIndexToLuid
	void __stdcall __E__26__()
	{
		PA = p[26];
		jumpToPA();
	}

	// ConvertRemoteInterfaceLuidToAlias
	void __stdcall __E__27__()
	{
		PA = p[27];
		jumpToPA();
	}

	// ConvertRemoteInterfaceLuidToGuid
	void __stdcall __E__28__()
	{
		PA = p[28];
		jumpToPA();
	}

	// ConvertRemoteInterfaceLuidToIndex
	void __stdcall __E__29__()
	{
		PA = p[29];
		jumpToPA();
	}

	// ConvertStringToGuidA
	void __stdcall __E__30__()
	{
		PA = p[30];
		jumpToPA();
	}

	// ConvertStringToGuidW
	void __stdcall __E__31__()
	{
		PA = p[31];
		jumpToPA();
	}

	// ConvertStringToInterfacePhysicalAddress
	void __stdcall __E__32__()
	{
		PA = p[32];
		jumpToPA();
	}

	// CreateAnycastIpAddressEntry
	void __stdcall __E__33__()
	{
		PA = p[33];
		jumpToPA();
	}

	// CreateIpForwardEntry
	void __stdcall __E__34__()
	{
		PA = p[34];
		jumpToPA();
	}

	// CreateIpForwardEntry2
	void __stdcall __E__35__()
	{
		PA = p[35];
		jumpToPA();
	}

	// CreateIpNetEntry
	void __stdcall __E__36__()
	{
		PA = p[36];
		jumpToPA();
	}

	// CreateIpNetEntry2
	void __stdcall __E__37__()
	{
		PA = p[37];
		jumpToPA();
	}

	// CreatePersistentTcpPortReservation
	void __stdcall __E__38__()
	{
		PA = p[38];
		jumpToPA();
	}

	// CreatePersistentUdpPortReservation
	void __stdcall __E__39__()
	{
		PA = p[39];
		jumpToPA();
	}

	// CreateProxyArpEntry
	void __stdcall __E__40__()
	{
		PA = p[40];
		jumpToPA();
	}

	// CreateSortedAddressPairs
	void __stdcall __E__41__()
	{
		PA = p[41];
		jumpToPA();
	}

	// CreateUnicastIpAddressEntry
	void __stdcall __E__42__()
	{
		PA = p[42];
		jumpToPA();
	}

	// DeleteAnycastIpAddressEntry
	void __stdcall __E__43__()
	{
		PA = p[43];
		jumpToPA();
	}

	// DeleteIPAddress
	void __stdcall __E__44__()
	{
		PA = p[44];
		jumpToPA();
	}

	// DeleteIpForwardEntry
	void __stdcall __E__45__()
	{
		PA = p[45];
		jumpToPA();
	}

	// DeleteIpForwardEntry2
	void __stdcall __E__46__()
	{
		PA = p[46];
		jumpToPA();
	}

	// DeleteIpNetEntry
	void __stdcall __E__47__()
	{
		PA = p[47];
		jumpToPA();
	}

	// DeleteIpNetEntry2
	void __stdcall __E__48__()
	{
		PA = p[48];
		jumpToPA();
	}

	// DeletePersistentTcpPortReservation
	void __stdcall __E__49__()
	{
		PA = p[49];
		jumpToPA();
	}

	// DeletePersistentUdpPortReservation
	void __stdcall __E__50__()
	{
		PA = p[50];
		jumpToPA();
	}

	// DeleteProxyArpEntry
	void __stdcall __E__51__()
	{
		PA = p[51];
		jumpToPA();
	}

	// DeleteUnicastIpAddressEntry
	void __stdcall __E__52__()
	{
		PA = p[52];
		jumpToPA();
	}

	// DisableMediaSense
	void __stdcall __E__53__()
	{
		PA = p[53];
		jumpToPA();
	}

	// EnableRouter
	void __stdcall __E__54__()
	{
		PA = p[54];
		jumpToPA();
	}

	// FlushIpNetTable
	void __stdcall __E__55__()
	{
		PA = p[55];
		jumpToPA();
	}

	// FlushIpNetTable2
	void __stdcall __E__56__()
	{
		PA = p[56];
		jumpToPA();
	}

	// FlushIpPathTable
	void __stdcall __E__57__()
	{
		PA = p[57];
		jumpToPA();
	}

	// FreeMibTable
	void __stdcall __E__58__()
	{
		PA = p[58];
		jumpToPA();
	}

	// GetAdapterIndex
	void __stdcall __E__59__()
	{
		PA = p[59];
		jumpToPA();
	}

	// GetAdapterOrderMap
	void __stdcall __E__60__()
	{
		PA = p[60];
		jumpToPA();
	}

	// GetAdaptersAddresses
	void __stdcall __E__61__()
	{
		PA = p[61];
		jumpToPA();
	}

	// GetAdaptersInfo
	void __stdcall __E__62__()
	{
		PA = p[62];
		jumpToPA();
	}

	// GetAnycastIpAddressEntry
	void __stdcall __E__63__()
	{
		PA = p[63];
		jumpToPA();
	}

	// GetAnycastIpAddressTable
	void __stdcall __E__64__()
	{
		PA = p[64];
		jumpToPA();
	}

	// GetBestInterface
	void __stdcall __E__65__()
	{
		PA = p[65];
		jumpToPA();
	}

	// GetBestInterfaceEx
	void __stdcall __E__66__()
	{
		PA = p[66];
		jumpToPA();
	}

	// GetBestRoute
	void __stdcall __E__67__()
	{
		PA = p[67];
		jumpToPA();
	}

	// GetBestRoute2
	void __stdcall __E__68__()
	{
		PA = p[68];
		jumpToPA();
	}

	// GetCurrentThreadCompartmentId
	void __stdcall __E__69__()
	{
		PA = p[69];
		jumpToPA();
	}

	// GetExtendedTcpTable
	void __stdcall __E__70__()
	{
		PA = p[70];
		jumpToPA();
	}

	// GetExtendedUdpTable
	void __stdcall __E__71__()
	{
		PA = p[71];
		jumpToPA();
	}

	// GetFriendlyIfIndex
	void __stdcall __E__72__()
	{
		PA = p[72];
		jumpToPA();
	}

	// GetIcmpStatistics
	void __stdcall __E__73__()
	{
		PA = p[73];
		jumpToPA();
	}

	// GetIcmpStatisticsEx
	void __stdcall __E__74__()
	{
		PA = p[74];
		jumpToPA();
	}

	// GetIfEntry
	void __stdcall __E__75__()
	{
		PA = p[75];
		jumpToPA();
	}

	// GetIfEntry2
	void __stdcall __E__76__()
	{
		PA = p[76];
		jumpToPA();
	}

	// GetIfStackTable
	void __stdcall __E__77__()
	{
		PA = p[77];
		jumpToPA();
	}

	// GetIfTable
	void __stdcall __E__78__()
	{
		PA = p[78];
		jumpToPA();
	}

	// GetIfTable2
	void __stdcall __E__79__()
	{
		PA = p[79];
		jumpToPA();
	}

	// GetIfTable2Ex
	void __stdcall __E__80__()
	{
		PA = p[80];
		jumpToPA();
	}

	// GetInterfaceInfo
	void __stdcall __E__81__()
	{
		PA = p[81];
		jumpToPA();
	}

	// GetInvertedIfStackTable
	void __stdcall __E__82__()
	{
		PA = p[82];
		jumpToPA();
	}

	// GetIpAddrTable
	void __stdcall __E__83__()
	{
		PA = p[83];
		jumpToPA();
	}

	// GetIpErrorString
	void __stdcall __E__84__()
	{
		PA = p[84];
		jumpToPA();
	}

	// GetIpForwardEntry2
	void __stdcall __E__85__()
	{
		PA = p[85];
		jumpToPA();
	}

	// GetIpForwardTable
	void __stdcall __E__86__()
	{
		PA = p[86];
		jumpToPA();
	}

	// GetIpForwardTable2
	void __stdcall __E__87__()
	{
		PA = p[87];
		jumpToPA();
	}

	// GetIpInterfaceEntry
	void __stdcall __E__88__()
	{
		PA = p[88];
		jumpToPA();
	}

	// GetIpInterfaceTable
	void __stdcall __E__89__()
	{
		PA = p[89];
		jumpToPA();
	}

	// GetIpNetEntry2
	void __stdcall __E__90__()
	{
		PA = p[90];
		jumpToPA();
	}

	// GetIpNetTable
	void __stdcall __E__91__()
	{
		PA = p[91];
		jumpToPA();
	}

	// GetIpNetTable2
	void __stdcall __E__92__()
	{
		PA = p[92];
		jumpToPA();
	}

	// GetIpNetworkConnectionBandwidthEstimates
	void __stdcall __E__93__()
	{
		PA = p[93];
		jumpToPA();
	}

	// GetIpPathEntry
	void __stdcall __E__94__()
	{
		PA = p[94];
		jumpToPA();
	}

	// GetIpPathTable
	void __stdcall __E__95__()
	{
		PA = p[95];
		jumpToPA();
	}

	// GetIpStatistics
	void __stdcall __E__96__()
	{
		PA = p[96];
		jumpToPA();
	}

	// GetIpStatisticsEx
	void __stdcall __E__97__()
	{
		PA = p[97];
		jumpToPA();
	}

	// GetMulticastIpAddressEntry
	void __stdcall __E__98__()
	{
		PA = p[98];
		jumpToPA();
	}

	// GetMulticastIpAddressTable
	void __stdcall __E__99__()
	{
		PA = p[99];
		jumpToPA();
	}

	// GetNetworkInformation
	void __stdcall __E__100__()
	{
		PA = p[100];
		jumpToPA();
	}

	// GetNetworkParams
	void __stdcall __E__101__()
	{
		PA = p[101];
		jumpToPA();
	}

	// GetNumberOfInterfaces
	void __stdcall __E__102__()
	{
		PA = p[102];
		jumpToPA();
	}

	// GetOwnerModuleFromPidAndInfo
	void __stdcall __E__103__()
	{
		PA = p[103];
		jumpToPA();
	}

	// GetOwnerModuleFromTcp6Entry
	void __stdcall __E__104__()
	{
		PA = p[104];
		jumpToPA();
	}

	// GetOwnerModuleFromTcpEntry
	void __stdcall __E__105__()
	{
		PA = p[105];
		jumpToPA();
	}

	// GetOwnerModuleFromUdp6Entry
	void __stdcall __E__106__()
	{
		PA = p[106];
		jumpToPA();
	}

	// GetOwnerModuleFromUdpEntry
	void __stdcall __E__107__()
	{
		PA = p[107];
		jumpToPA();
	}

	// GetPerAdapterInfo
	void __stdcall __E__108__()
	{
		PA = p[108];
		jumpToPA();
	}

	// GetPerTcp6ConnectionEStats
	void __stdcall __E__109__()
	{
		PA = p[109];
		jumpToPA();
	}

	// GetPerTcp6ConnectionStats
	void __stdcall __E__110__()
	{
		PA = p[110];
		jumpToPA();
	}

	// GetPerTcpConnectionEStats
	void __stdcall __E__111__()
	{
		PA = p[111];
		jumpToPA();
	}

	// GetPerTcpConnectionStats
	void __stdcall __E__112__()
	{
		PA = p[112];
		jumpToPA();
	}

	// GetRTTAndHopCount
	void __stdcall __E__113__()
	{
		PA = p[113];
		jumpToPA();
	}

	// GetSessionCompartmentId
	void __stdcall __E__114__()
	{
		PA = p[114];
		jumpToPA();
	}

	// GetTcp6Table
	void __stdcall __E__115__()
	{
		PA = p[115];
		jumpToPA();
	}

	// GetTcp6Table2
	void __stdcall __E__116__()
	{
		PA = p[116];
		jumpToPA();
	}

	// GetTcpStatistics
	void __stdcall __E__117__()
	{
		PA = p[117];
		jumpToPA();
	}

	// GetTcpStatisticsEx
	void __stdcall __E__118__()
	{
		PA = p[118];
		jumpToPA();
	}

	// GetTcpTable
	void __stdcall __E__119__()
	{
		PA = p[119];
		jumpToPA();
	}

	// GetTcpTable2
	void __stdcall __E__120__()
	{
		PA = p[120];
		jumpToPA();
	}

	// GetTeredoPort
	void __stdcall __E__121__()
	{
		PA = p[121];
		jumpToPA();
	}

	// GetUdp6Table
	void __stdcall __E__122__()
	{
		PA = p[122];
		jumpToPA();
	}

	// GetUdpStatistics
	void __stdcall __E__123__()
	{
		PA = p[123];
		jumpToPA();
	}

	// GetUdpStatisticsEx
	void __stdcall __E__124__()
	{
		PA = p[124];
		jumpToPA();
	}

	// GetUdpTable
	void __stdcall __E__125__()
	{
		PA = p[125];
		jumpToPA();
	}

	// GetUniDirectionalAdapterInfo
	void __stdcall __E__126__()
	{
		PA = p[126];
		jumpToPA();
	}

	// GetUnicastIpAddressEntry
	void __stdcall __E__127__()
	{
		PA = p[127];
		jumpToPA();
	}

	// GetUnicastIpAddressTable
	void __stdcall __E__128__()
	{
		PA = p[128];
		jumpToPA();
	}

	// Icmp6CreateFile
	void __stdcall __E__129__()
	{
		PA = p[129];
		jumpToPA();
	}

	// Icmp6ParseReplies
	void __stdcall __E__130__()
	{
		PA = p[130];
		jumpToPA();
	}

	// Icmp6SendEcho2
	void __stdcall __E__131__()
	{
		PA = p[131];
		jumpToPA();
	}

	// IcmpCloseHandle
	void __stdcall __E__132__()
	{
		PA = p[132];
		jumpToPA();
	}

	// IcmpCreateFile
	void __stdcall __E__133__()
	{
		PA = p[133];
		jumpToPA();
	}

	// IcmpParseReplies
	void __stdcall __E__134__()
	{
		PA = p[134];
		jumpToPA();
	}

	// IcmpSendEcho
	void __stdcall __E__135__()
	{
		PA = p[135];
		jumpToPA();
	}

	// IcmpSendEcho2
	void __stdcall __E__136__()
	{
		PA = p[136];
		jumpToPA();
	}

	// IcmpSendEcho2Ex
	void __stdcall __E__137__()
	{
		PA = p[137];
		jumpToPA();
	}

	// InitializeIpForwardEntry
	void __stdcall __E__138__()
	{
		PA = p[138];
		jumpToPA();
	}

	// InitializeIpInterfaceEntry
	void __stdcall __E__139__()
	{
		PA = p[139];
		jumpToPA();
	}

	// InitializeUnicastIpAddressEntry
	void __stdcall __E__140__()
	{
		PA = p[140];
		jumpToPA();
	}

	// InternalCleanupPersistentStore
	void __stdcall __E__141__()
	{
		PA = p[141];
		jumpToPA();
	}

	// InternalCreateAnycastIpAddressEntry
	void __stdcall __E__142__()
	{
		PA = p[142];
		jumpToPA();
	}

	// InternalCreateIpForwardEntry
	void __stdcall __E__143__()
	{
		PA = p[143];
		jumpToPA();
	}

	// InternalCreateIpForwardEntry2
	void __stdcall __E__144__()
	{
		PA = p[144];
		jumpToPA();
	}

	// InternalCreateIpNetEntry
	void __stdcall __E__145__()
	{
		PA = p[145];
		jumpToPA();
	}

	// InternalCreateIpNetEntry2
	void __stdcall __E__146__()
	{
		PA = p[146];
		jumpToPA();
	}

	// InternalCreateUnicastIpAddressEntry
	void __stdcall __E__147__()
	{
		PA = p[147];
		jumpToPA();
	}

	// InternalDeleteAnycastIpAddressEntry
	void __stdcall __E__148__()
	{
		PA = p[148];
		jumpToPA();
	}

	// InternalDeleteIpForwardEntry
	void __stdcall __E__149__()
	{
		PA = p[149];
		jumpToPA();
	}

	// InternalDeleteIpForwardEntry2
	void __stdcall __E__150__()
	{
		PA = p[150];
		jumpToPA();
	}

	// InternalDeleteIpNetEntry
	void __stdcall __E__151__()
	{
		PA = p[151];
		jumpToPA();
	}

	// InternalDeleteIpNetEntry2
	void __stdcall __E__152__()
	{
		PA = p[152];
		jumpToPA();
	}

	// InternalDeleteUnicastIpAddressEntry
	void __stdcall __E__153__()
	{
		PA = p[153];
		jumpToPA();
	}

	// InternalFindInterfaceByAddress
	void __stdcall __E__154__()
	{
		PA = p[154];
		jumpToPA();
	}

	// InternalGetAnycastIpAddressEntry
	void __stdcall __E__155__()
	{
		PA = p[155];
		jumpToPA();
	}

	// InternalGetAnycastIpAddressTable
	void __stdcall __E__156__()
	{
		PA = p[156];
		jumpToPA();
	}

	// InternalGetForwardIpTable2
	void __stdcall __E__157__()
	{
		PA = p[157];
		jumpToPA();
	}

	// InternalGetIPPhysicalInterfaceForDestination
	void __stdcall __E__158__()
	{
		PA = p[158];
		jumpToPA();
	}

	// InternalGetIfEntry2
	void __stdcall __E__159__()
	{
		PA = p[159];
		jumpToPA();
	}

	// InternalGetIfTable
	void __stdcall __E__160__()
	{
		PA = p[160];
		jumpToPA();
	}

	// InternalGetIfTable2
	void __stdcall __E__161__()
	{
		PA = p[161];
		jumpToPA();
	}

	// InternalGetIpAddrTable
	void __stdcall __E__162__()
	{
		PA = p[162];
		jumpToPA();
	}

	// InternalGetIpForwardEntry2
	void __stdcall __E__163__()
	{
		PA = p[163];
		jumpToPA();
	}

	// InternalGetIpForwardTable
	void __stdcall __E__164__()
	{
		PA = p[164];
		jumpToPA();
	}

	// InternalGetIpInterfaceEntry
	void __stdcall __E__165__()
	{
		PA = p[165];
		jumpToPA();
	}

	// InternalGetIpInterfaceTable
	void __stdcall __E__166__()
	{
		PA = p[166];
		jumpToPA();
	}

	// InternalGetIpNetEntry2
	void __stdcall __E__167__()
	{
		PA = p[167];
		jumpToPA();
	}

	// InternalGetIpNetTable
	void __stdcall __E__168__()
	{
		PA = p[168];
		jumpToPA();
	}

	// InternalGetIpNetTable2
	void __stdcall __E__169__()
	{
		PA = p[169];
		jumpToPA();
	}

	// InternalGetMulticastIpAddressEntry
	void __stdcall __E__170__()
	{
		PA = p[170];
		jumpToPA();
	}

	// InternalGetMulticastIpAddressTable
	void __stdcall __E__171__()
	{
		PA = p[171];
		jumpToPA();
	}

	// InternalGetRtcSlotInformation
	void __stdcall __E__172__()
	{
		PA = p[172];
		jumpToPA();
	}

	// InternalGetTcp6Table2
	void __stdcall __E__173__()
	{
		PA = p[173];
		jumpToPA();
	}

	// InternalGetTcp6TableWithOwnerModule
	void __stdcall __E__174__()
	{
		PA = p[174];
		jumpToPA();
	}

	// InternalGetTcp6TableWithOwnerPid
	void __stdcall __E__175__()
	{
		PA = p[175];
		jumpToPA();
	}

	// InternalGetTcpTable
	void __stdcall __E__176__()
	{
		PA = p[176];
		jumpToPA();
	}

	// InternalGetTcpTable2
	void __stdcall __E__177__()
	{
		PA = p[177];
		jumpToPA();
	}

	// InternalGetTcpTableEx
	void __stdcall __E__178__()
	{
		PA = p[178];
		jumpToPA();
	}

	// InternalGetTcpTableWithOwnerModule
	void __stdcall __E__179__()
	{
		PA = p[179];
		jumpToPA();
	}

	// InternalGetTcpTableWithOwnerPid
	void __stdcall __E__180__()
	{
		PA = p[180];
		jumpToPA();
	}

	// InternalGetTunnelPhysicalAdapter
	void __stdcall __E__181__()
	{
		PA = p[181];
		jumpToPA();
	}

	// InternalGetUdp6TableWithOwnerModule
	void __stdcall __E__182__()
	{
		PA = p[182];
		jumpToPA();
	}

	// InternalGetUdp6TableWithOwnerPid
	void __stdcall __E__183__()
	{
		PA = p[183];
		jumpToPA();
	}

	// InternalGetUdpTable
	void __stdcall __E__184__()
	{
		PA = p[184];
		jumpToPA();
	}

	// InternalGetUdpTableEx
	void __stdcall __E__185__()
	{
		PA = p[185];
		jumpToPA();
	}

	// InternalGetUdpTableWithOwnerModule
	void __stdcall __E__186__()
	{
		PA = p[186];
		jumpToPA();
	}

	// InternalGetUdpTableWithOwnerPid
	void __stdcall __E__187__()
	{
		PA = p[187];
		jumpToPA();
	}

	// InternalGetUnicastIpAddressEntry
	void __stdcall __E__188__()
	{
		PA = p[188];
		jumpToPA();
	}

	// InternalGetUnicastIpAddressTable
	void __stdcall __E__189__()
	{
		PA = p[189];
		jumpToPA();
	}

	// InternalIcmpCreateFileEx
	void __stdcall __E__190__()
	{
		PA = p[190];
		jumpToPA();
	}

	// InternalSetIfEntry
	void __stdcall __E__191__()
	{
		PA = p[191];
		jumpToPA();
	}

	// InternalSetIpForwardEntry
	void __stdcall __E__192__()
	{
		PA = p[192];
		jumpToPA();
	}

	// InternalSetIpForwardEntry2
	void __stdcall __E__193__()
	{
		PA = p[193];
		jumpToPA();
	}

	// InternalSetIpInterfaceEntry
	void __stdcall __E__194__()
	{
		PA = p[194];
		jumpToPA();
	}

	// InternalSetIpNetEntry
	void __stdcall __E__195__()
	{
		PA = p[195];
		jumpToPA();
	}

	// InternalSetIpNetEntry2
	void __stdcall __E__196__()
	{
		PA = p[196];
		jumpToPA();
	}

	// InternalSetIpStats
	void __stdcall __E__197__()
	{
		PA = p[197];
		jumpToPA();
	}

	// InternalSetTcpEntry
	void __stdcall __E__198__()
	{
		PA = p[198];
		jumpToPA();
	}

	// InternalSetTeredoPort
	void __stdcall __E__199__()
	{
		PA = p[199];
		jumpToPA();
	}

	// InternalSetUnicastIpAddressEntry
	void __stdcall __E__200__()
	{
		PA = p[200];
		jumpToPA();
	}

	// IpReleaseAddress
	void __stdcall __E__201__()
	{
		PA = p[201];
		jumpToPA();
	}

	// IpRenewAddress
	void __stdcall __E__202__()
	{
		PA = p[202];
		jumpToPA();
	}

	// LookupPersistentTcpPortReservation
	void __stdcall __E__203__()
	{
		PA = p[203];
		jumpToPA();
	}

	// LookupPersistentUdpPortReservation
	void __stdcall __E__204__()
	{
		PA = p[204];
		jumpToPA();
	}

	// NTPTimeToNTFileTime
	void __stdcall __E__205__()
	{
		PA = p[205];
		jumpToPA();
	}

	// NTTimeToNTPTime
	void __stdcall __E__206__()
	{
		PA = p[206];
		jumpToPA();
	}

	// NhGetGuidFromInterfaceName
	void __stdcall __E__207__()
	{
		PA = p[207];
		jumpToPA();
	}

	// NhGetInterfaceDescriptionFromGuid
	void __stdcall __E__208__()
	{
		PA = p[208];
		jumpToPA();
	}

	// NhGetInterfaceNameFromDeviceGuid
	void __stdcall __E__209__()
	{
		PA = p[209];
		jumpToPA();
	}

	// NhGetInterfaceNameFromGuid
	void __stdcall __E__210__()
	{
		PA = p[210];
		jumpToPA();
	}

	// NhpAllocateAndGetInterfaceInfoFromStack
	void __stdcall __E__211__()
	{
		PA = p[211];
		jumpToPA();
	}

	// NotifyAddrChange
	void __stdcall __E__212__()
	{
		PA = p[212];
		jumpToPA();
	}

	// NotifyCompartmentChange
	void __stdcall __E__213__()
	{
		PA = p[213];
		jumpToPA();
	}

	// NotifyIpInterfaceChange
	void __stdcall __E__214__()
	{
		PA = p[214];
		jumpToPA();
	}

	// NotifyRouteChange
	void __stdcall __E__215__()
	{
		PA = p[215];
		jumpToPA();
	}

	// NotifyRouteChange2
	void __stdcall __E__216__()
	{
		PA = p[216];
		jumpToPA();
	}

	// NotifyStableUnicastIpAddressTable
	void __stdcall __E__217__()
	{
		PA = p[217];
		jumpToPA();
	}

	// NotifyTeredoPortChange
	void __stdcall __E__218__()
	{
		PA = p[218];
		jumpToPA();
	}

	// NotifyUnicastIpAddressChange
	void __stdcall __E__219__()
	{
		PA = p[219];
		jumpToPA();
	}

	// OpenCompartment
	void __stdcall __E__220__()
	{
		PA = p[220];
		jumpToPA();
	}

	// ParseNetworkString
	void __stdcall __E__221__()
	{
		PA = p[221];
		jumpToPA();
	}

	// ResolveIpNetEntry2
	void __stdcall __E__222__()
	{
		PA = p[222];
		jumpToPA();
	}

	// ResolveNeighbor
	void __stdcall __E__223__()
	{
		PA = p[223];
		jumpToPA();
	}

	// RestoreMediaSense
	void __stdcall __E__224__()
	{
		PA = p[224];
		jumpToPA();
	}

	// SendARP
	void __stdcall __E__225__()
	{
		PA = p[225];
		jumpToPA();
	}

	// SetAdapterIpAddress
	void __stdcall __E__226__()
	{
		PA = p[226];
		jumpToPA();
	}

	// SetCurrentThreadCompartmentId
	void __stdcall __E__227__()
	{
		PA = p[227];
		jumpToPA();
	}

	// SetIfEntry
	void __stdcall __E__228__()
	{
		PA = p[228];
		jumpToPA();
	}

	// SetIpForwardEntry
	void __stdcall __E__229__()
	{
		PA = p[229];
		jumpToPA();
	}

	// SetIpForwardEntry2
	void __stdcall __E__230__()
	{
		PA = p[230];
		jumpToPA();
	}

	// SetIpInterfaceEntry
	void __stdcall __E__231__()
	{
		PA = p[231];
		jumpToPA();
	}

	// SetIpNetEntry
	void __stdcall __E__232__()
	{
		PA = p[232];
		jumpToPA();
	}

	// SetIpNetEntry2
	void __stdcall __E__233__()
	{
		PA = p[233];
		jumpToPA();
	}

	// SetIpStatistics
	void __stdcall __E__234__()
	{
		PA = p[234];
		jumpToPA();
	}

	// SetIpStatisticsEx
	void __stdcall __E__235__()
	{
		PA = p[235];
		jumpToPA();
	}

	// SetIpTTL
	void __stdcall __E__236__()
	{
		PA = p[236];
		jumpToPA();
	}

	// SetNetworkInformation
	void __stdcall __E__237__()
	{
		PA = p[237];
		jumpToPA();
	}

	// SetPerTcp6ConnectionEStats
	void __stdcall __E__238__()
	{
		PA = p[238];
		jumpToPA();
	}

	// SetPerTcp6ConnectionStats
	void __stdcall __E__239__()
	{
		PA = p[239];
		jumpToPA();
	}

	// SetPerTcpConnectionEStats
	void __stdcall __E__240__()
	{
		PA = p[240];
		jumpToPA();
	}

	// SetPerTcpConnectionStats
	void __stdcall __E__241__()
	{
		PA = p[241];
		jumpToPA();
	}

	// SetSessionCompartmentId
	void __stdcall __E__242__()
	{
		PA = p[242];
		jumpToPA();
	}

	// SetTcpEntry
	void __stdcall __E__243__()
	{
		PA = p[243];
		jumpToPA();
	}

	// SetUnicastIpAddressEntry
	void __stdcall __E__244__()
	{
		PA = p[244];
		jumpToPA();
	}

	// UnenableRouter
	void __stdcall __E__245__()
	{
		PA = p[245];
		jumpToPA();
	}

	// _PfAddFiltersToInterface@24
	void __stdcall __E__246__()
	{
		PA = p[246];
		jumpToPA();
	}

	// _PfAddGlobalFilterToInterface@8
	void __stdcall __E__247__()
	{
		PA = p[247];
		jumpToPA();
	}

	// _PfBindInterfaceToIPAddress@12
	void __stdcall __E__248__()
	{
		PA = p[248];
		jumpToPA();
	}

	// _PfBindInterfaceToIndex@16
	void __stdcall __E__249__()
	{
		PA = p[249];
		jumpToPA();
	}

	// _PfCreateInterface@24
	void __stdcall __E__250__()
	{
		PA = p[250];
		jumpToPA();
	}

	// _PfDeleteInterface@4
	void __stdcall __E__251__()
	{
		PA = p[251];
		jumpToPA();
	}

	// _PfDeleteLog@0
	void __stdcall __E__252__()
	{
		PA = p[252];
		jumpToPA();
	}

	// _PfGetInterfaceStatistics@16
	void __stdcall __E__253__()
	{
		PA = p[253];
		jumpToPA();
	}

	// _PfMakeLog@4
	void __stdcall __E__254__()
	{
		PA = p[254];
		jumpToPA();
	}

	// _PfRebindFilters@8
	void __stdcall __E__255__()
	{
		PA = p[255];
		jumpToPA();
	}

	// _PfRemoveFilterHandles@12
	void __stdcall __E__256__()
	{
		PA = p[256];
		jumpToPA();
	}

	// _PfRemoveFiltersFromInterface@20
	void __stdcall __E__257__()
	{
		PA = p[257];
		jumpToPA();
	}

	// _PfRemoveGlobalFilterFromInterface@8
	void __stdcall __E__258__()
	{
		PA = p[258];
		jumpToPA();
	}

	// _PfSetLogBuffer@28
	void __stdcall __E__259__()
	{
		PA = p[259];
		jumpToPA();
	}

	// _PfTestPacket@20
	void __stdcall __E__260__()
	{
		PA = p[260];
		jumpToPA();
	}

	// _PfUnBindInterface@4
	void __stdcall __E__261__()
	{
		PA = p[261];
		jumpToPA();
	}

	// do_echo_rep
	void __stdcall __E__262__()
	{
		PA = p[262];
		jumpToPA();
	}

	// do_echo_req
	void __stdcall __E__263__()
	{
		PA = p[263];
		jumpToPA();
	}

	// if_indextoname
	void __stdcall __E__264__()
	{
		PA = p[264];
		jumpToPA();
	}

	// if_nametoindex
	void __stdcall __E__265__()
	{
		PA = p[265];
		jumpToPA();
	}

	// register_icmp
	void __stdcall __E__266__()
	{
		PA = p[266];
		jumpToPA();
	}
}

#endif